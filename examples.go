/*
 * Copyright (c) 2017-2022 Alex <mailoman2008@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author Alex <mailoman2008@gmail.com>
 * @copyright Copyright (c) 2017-2022 Alex <mailoman2008@gmail.com>
 */

package main

import (
	"fmt"

	"gitlab.com/mailoman/go-utils/examples"
	"gitlab.com/mailoman/go-utils/mapping"
)

func main() {

	// Example 1, no strict mapping rules at all
	in := examples.InputExample1{
		Str: "1",
		I32: 32,
		I64: 64,
		Boo: true,
		F32: 32.23,
	}

	out := &examples.OutputExample1{}

	// Simple usage
	mapping.MapAllFields(in, out, nil)
	fmt.Printf("%+v ==> %+v\n", in, *out)
}
