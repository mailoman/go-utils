/*
 * Copyright (c) 2017-2022 Alex <mailoman2008@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author Alex <mailoman2008@gmail.com>
 * @copyright Copyright (c) 2017-2022 Alex <mailoman2008@gmail.com>
 */

package examples

// Input structure
type InputExample1 struct {
	Str string
	I32 int32
	I64 int64
	Boo bool
	F32 float32
}

// Output structure
type OutputExample1 struct {
	Str float64
	I32 string
	I64 string
	Boo int
	F32 int64
}
