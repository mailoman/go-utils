# go-utils

[![Join the chat at https://gitter.im/go_utils/Lobby](https://badges.gitter.im/go_utils/Lobby.svg)](https://gitter.im/go_utils/Lobby?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)
[![pipeline](http://gitlab.com/mailoman/go-utils/badges/master/pipeline.svg)](http://gitlab.com/mailoman/go-utils/commits/master)
[![Coverage Status](https://coveralls.io/repos/gitlab/mailoman/go-utils/badge.svg)](https://coveralls.io/github/mailoman/go-utils)
[![GoDoc](https://godoc.org/gitlab.com/mailoman/go-utils/mapping?status.svg)](https://godoc.org/gitlab.com/mailoman/go-utils/mapping)
[![coverage report](http://gitlab.com/mailoman/go-utils/badges/master/coverage.svg?job=coverage)](http://gitlab.com/mailoman/go-utils/commits/master)

## License

Source code is licensed under the [Apache Licence, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.html).

Copyright (c) 2017-2022 Alex <mailoman2008@gmail.com>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
